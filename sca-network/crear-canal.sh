export CORE_PEER_TLS_ENABLED=true
export ORDERER_CA=${PWD}/canal/crypto-config/ordererOrganizations/sca.org/orderers/orderer.sca.org/msp/tlscacerts/tlsca.sca.org-cert.pem
export PEER0_ORG1_CA=${PWD}/canal/crypto-config/peerOrganizations/org1.sca.org/peers/peer0.org1.sca.org/tls/ca.crt
export PEER0_ORG2_CA=${PWD}/canal/crypto-config/peerOrganizations/org2.sca.org/peers/peer0.org2.sca.org/tls/ca.crt
export PEER0_ORG3_CA=${PWD}/canal/crypto-config/peerOrganizations/org3.sca.org/peers/peer0.org3.sca.org/tls/ca.crt
export FABRIC_CFG_PATH=${PWD}/canal/config/

export CHANNEL_NAME=universidad

setGlobalsForPeer0Org1(){
    export CORE_PEER_LOCALMSPID="Org1MSP"
    export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_ORG1_CA
    export CORE_PEER_MSPCONFIGPATH=${PWD}/canal/crypto-config/peerOrganizations/org1.sca.org/users/Admin@org1.sca.org/msp
    export CORE_PEER_ADDRESS=localhost:7051
}


setGlobalsForPeer0Org2(){
    export CORE_PEER_LOCALMSPID="Org2MSP"
    export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_ORG2_CA
    export CORE_PEER_MSPCONFIGPATH=${PWD}/canal/crypto-config/peerOrganizations/org2.sca.org/users/Admin@org2.sca.org/msp
    export CORE_PEER_ADDRESS=localhost:8051
}

setGlobalsForPeer0Org3(){
    export CORE_PEER_LOCALMSPID="Org3MSP"
    export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_ORG3_CA
    export CORE_PEER_MSPCONFIGPATH=${PWD}/canal/crypto-config/peerOrganizations/org3.sca.org/users/Admin@org3.sca.org/msp
    export CORE_PEER_ADDRESS=localhost:9051
}

createChannel(){
    echo "#######    Ceando el canal  ##########"
    setGlobalsForPeer0Org1
    
    peer channel create -o localhost:7050 -c $CHANNEL_NAME \
    --ordererTLSHostnameOverride orderer.sca.org \
    -f ./channel-artifacts/channel.tx --outputBlock ./channel-artifacts/${CHANNEL_NAME}.block \
    --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA
}

joinChannel(){
    echo "#######    Uniendo los peers al canal  ##########"
    setGlobalsForPeer0Org1
    peer channel join -b ./channel-artifacts/$CHANNEL_NAME.block
    
    setGlobalsForPeer0Org2
    peer channel join -b ./channel-artifacts/$CHANNEL_NAME.block

    setGlobalsForPeer0Org3
    peer channel join -b ./channel-artifacts/$CHANNEL_NAME.block
}

updateAnchorPeers(){
    echo "#######    Actualizando los anchor peers  ##########"
    setGlobalsForPeer0Org1
    peer channel update -o localhost:7050 --ordererTLSHostnameOverride orderer.sca.org \
    -c $CHANNEL_NAME -f ./channel-artifacts/${CORE_PEER_LOCALMSPID}anchors.tx \
    --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA
    
    setGlobalsForPeer0Org2
    peer channel update -o localhost:7050 --ordererTLSHostnameOverride orderer.sca.org \
    -c $CHANNEL_NAME -f ./channel-artifacts/${CORE_PEER_LOCALMSPID}anchors.tx \
    --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA

    setGlobalsForPeer0Org3
    peer channel update -o localhost:7050 --ordererTLSHostnameOverride orderer.sca.org \
    -c $CHANNEL_NAME -f ./channel-artifacts/${CORE_PEER_LOCALMSPID}anchors.tx \
    --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA
}

createChannel
joinChannel
updateAnchorPeers