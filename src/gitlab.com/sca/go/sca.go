/*
Business Blockchain Training & Consulting SpA. All Rights Reserved.
www.blockchainempresarial.com
email: ricardo@blockchainempresarial.com
*/

package main

import (
	"encoding/json"
	"fmt"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

// Definimos el Smartcontract para Rol
type RolContract struct {
	contractapi.Contract
}

// Definimos el Smartcontract para Servicio
type ServicioContract struct {
	contractapi.Contract
}

// Definimos el Smartcontract para Usuario
type UsuarioContract struct {
	contractapi.Contract
}

//Definimos la estructura de datos para el Rol
type Rol struct {
	ID           string `json:"id"`
	Nombre       string `json:"nombre"`
	Organizacion string `json:"organizacion"`
	Descripcion  string `json:"descripcion"`
	Estado       string `json:"estado"`
}

//Definimos la estructura de datos para los Servicios
type Servicio struct {
	ID           string `json:"id"`
	Url          string `json:"url"`
	Operaciones  string `json:"operaciones"`
	Rol          string `json:"rol"`
	Organizacion string `json:"organizacion"`
	Descripcion  string `json:"descripcion"`
	Estado       string `json:"estado"`
}

//Definimos la estructura de datos para el Usuario
type Usuario struct {
	ID     string `json:"id"`
	Rol    string `json:"rol"`
	Estado string `json:"estado"`
}

//Funcion para registrar un rol.
func (s *RolContract) RegistrarRol(ctx contractapi.TransactionContextInterface, rolId string, nombre string, org string, desc string, estado string) (string, error) {

	rol := Rol{
		ID:           rolId,
		Nombre:       nombre,
		Organizacion: org,
		Descripcion:  desc,
		Estado:       estado,
	}

	rolAsBytes, err := json.Marshal(rol)
	if err != nil {
		return "", fmt.Errorf("failed while marshling Rol. %s", err.Error())
	}

	return ctx.GetStub().GetTxID(), ctx.GetStub().PutState(rol.ID, rolAsBytes)
}

//Funcion para obtener un Rol dado su id.
func (s *RolContract) ObtenerRol(ctx contractapi.TransactionContextInterface, rolId string) (*Rol, error) {
	if len(rolId) == 0 {
		return nil, fmt.Errorf("please provide correct rol Id")
		// return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	rolAsBytes, err := ctx.GetStub().GetState(rolId)

	if err != nil {
		return nil, fmt.Errorf("failed to read from world state. %s", err.Error())
	}

	if rolAsBytes == nil {
		return nil, fmt.Errorf("%s does not exist", rolId)
	}

	rol := new(Rol)
	_ = json.Unmarshal(rolAsBytes, rol)

	return rol, nil
}

// Funcion para regitrar un servicio
func (s *ServicioContract) RegistarServicio(ctx contractapi.TransactionContextInterface, servicioId string, url string, ops string, rol string, org string, desc string, estado string) (string, error) {

	servicio := Servicio{
		ID:           servicioId,
		Url:          url,
		Operaciones:  ops,
		Rol:          rol,
		Organizacion: org,
		Descripcion:  desc,
		Estado:       estado,
	}

	servicioAsBytes, err := json.Marshal(servicio)
	if err != nil {
		return "", fmt.Errorf("failed while marshling Servicio. %s", err.Error())
	}

	return ctx.GetStub().GetTxID(), ctx.GetStub().PutState(servicio.ID, servicioAsBytes)
}

//Funcion para obtener un Servicio dado su url.
func (s *ServicioContract) ObtenerServicio(ctx contractapi.TransactionContextInterface, servicioId string) (*Servicio, error) {
	if len(servicioId) == 0 {
		return nil, fmt.Errorf("please provide correct rol Id")
		// return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	servicioAsBytes, err := ctx.GetStub().GetState(servicioId)

	if err != nil {
		return nil, fmt.Errorf("failed to read from world state. %s", err.Error())
	}

	if servicioAsBytes == nil {
		return nil, fmt.Errorf("%s does not exist", servicioId)
	}

	servicio := new(Servicio)
	_ = json.Unmarshal(servicioAsBytes, servicio)

	return servicio, nil
}

//Funcion para registrar un usuario con rol en especifico.
func (s *UsuarioContract) RegistrarUsuario(ctx contractapi.TransactionContextInterface, usuarioId string, rolId string, estado string) (string, error) {

	usuario := Usuario{
		ID:     usuarioId,
		Rol:    rolId,
		Estado: estado,
	}

	usuarioAsBytes, err := json.Marshal(usuario)
	if err != nil {
		return "", fmt.Errorf("failed while marshling Usuario. %s", err.Error())
	}

	return ctx.GetStub().GetTxID(), ctx.GetStub().PutState(usuarioId, usuarioAsBytes)
}

//Funcion para obtener un usuario mediante su id.
func (s *UsuarioContract) ObtenerUsuario(ctx contractapi.TransactionContextInterface, usuarioId string) (*Usuario, error) {

	usuarioAsBytes, err := ctx.GetStub().GetState(usuarioId)

	if err != nil {
		return nil, fmt.Errorf("failed to read from world state. %s", err.Error())
	}

	if usuarioAsBytes == nil {
		return nil, fmt.Errorf("%s does not exist", usuarioId)
	}

	usuario := new(Usuario)

	err = json.Unmarshal(usuarioAsBytes, usuario)
	if err != nil {
		return nil, fmt.Errorf("unmarshal error. %s", err.Error())
	}

	return usuario, nil
}

//Funcion para obtener rol
func (s *UsuarioContract) ObtenerRolUsuario(ctx contractapi.TransactionContextInterface, rolId string) (string, error) {

	if len(rolId) == 0 {
		return "", fmt.Errorf("please provide correct contract Id")
		// return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	params := []string{"RolContract:ObtenerRol", rolId}
	queryArgs := make([][]byte, len(params))
	for i, arg := range params {
		queryArgs[i] = []byte(arg)
	}

	response := ctx.GetStub().InvokeChaincode("RolContract", queryArgs, "universidad")

	return string(response.Payload), nil
}

func main() {

	chaincode, err := contractapi.NewChaincode(new(RolContract), new(ServicioContract), new(UsuarioContract))

	if err != nil {
		fmt.Printf("Error create sca chaincode: %s", err.Error())
		return
	}

	if err := chaincode.Start(); err != nil {
		fmt.Printf("Error starting chaincodes: %s", err.Error())
	}
}
