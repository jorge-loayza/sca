'use strict';

var { Wallets } = require('fabric-network');
const path = require('path');
const FabricCAServices = require('fabric-ca-client');
const fs = require('fs');

const getCCP = async (org) => {
  let ccpPath = null;

  org == 'Org1'
    ? (ccpPath = path.resolve(
        __dirname,
        '..',
        'config',
        'connection-org1.json'
      ))
    : null;
  org == 'Org2'
    ? (ccpPath = path.resolve(
        __dirname,
        '..',
        'config',
        'connection-org2.json'
      ))
    : null;
  org == 'Org3'
    ? (ccpPath = path.resolve(
        __dirname,
        '..',
        'config',
        'connection-org3.json'
      ))
    : null;
  const ccpJSON = fs.readFileSync(ccpPath, 'utf8');

  const ccp = JSON.parse(ccpJSON);
  return ccp;
};

const getWalletPath = async (org) => {
  let walletPath = null;
  org == 'Org1'
    ? (walletPath = path.join(process.cwd(), 'helpers/org1-wallet'))
    : null;
  org == 'Org2' ? (walletPath = path.join(process.cwd(), 'org2-wallet')) : null;
  org == 'Org3' ? (walletPath = path.join(process.cwd(), 'org3-wallet')) : null;
  return walletPath;
};

const isUserRegistered = async (username, userOrg) => {
  const walletPath = await getWalletPath(userOrg);
  const wallet = await Wallets.newFileSystemWallet(walletPath);
  console.log(`Wallet path: ${walletPath}`);

  const userIdentity = await wallet.get(username);
  if (userIdentity) {
    console.log(`An identity for the user ${username} exists in the wallet`);
    return true;
  }
  return false;
};

const getCaInfo = async (org, ccp) => {
  let caInfo = null;
  org == 'Org1'
    ? (caInfo = ccp.certificateAuthorities['ca.org1.sca.org'])
    : null;
  org == 'Org2'
    ? (caInfo = ccp.certificateAuthorities['ca.org2.sca.org'])
    : null;
  org == 'Org3'
    ? (caInfo = ccp.certificateAuthorities['ca.org3.sca.org'])
    : null;
  return caInfo;
};

const enrollAdmin = async (org) => {
  let ccp = await getCCP(org);
  console.log('Llamando al metodo: enrollAdim');
  try {
    const caInfo = await getCaInfo(org, ccp);
    const caTLSCACerts = caInfo.tlsCACerts.pem;
    const ca = new FabricCAServices(
      caInfo.url,
      { trustedRoots: caTLSCACerts, verify: false },
      caInfo.caName
    );

    const walletPath = await getWalletPath(org);
    const wallet = await Wallets.newFileSystemWallet(walletPath);
    console.log(`Wallet path: ${walletPath}`);

    // Verificamos de nuevo si existe la identidad para el administrador
    const identity = await wallet.get('admin');
    if (identity) {
      console.log('La identidad para el Adminstrador ya existe en el wallet');
      return;
    }

    // Enroll del adminstrador y importacion de la nueva identidad en el wallet
    const enrollment = await ca.enroll({
      enrollmentID: 'admin',
      enrollmentSecret: 'adminpw',
    });
    console.log('Enrollment object is : ', enrollment);
    let x509Identity = {
      credentials: {
        certificate: enrollment.certificate,
        privateKey: enrollment.key.toBytes(),
      },
      mspId: `${org}MSP`,
      type: 'X.509',
    };

    await wallet.put('admin', x509Identity);
    console.log('Admin enrollment exitoso e importado en el wallet');
    return;
  } catch (error) {
    console.error(`Fallo en el enroll del administrador: ${error}`);
  }
};

module.exports = {
  getCCP: getCCP,
  getWalletPath: getWalletPath,
  enrollAdmin,
};
