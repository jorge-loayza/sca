const express = require('express');

const { registrarRol, obtenerRol } = require('../controllers/rol');

const router = express.Router();

router
  .route('/channels/:channelName/chaincodes/:chaincodeName')
  .get(obtenerRol)
  .post(registrarRol);

module.exports = router;
