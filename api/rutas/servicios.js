const express = require('express');

const {
  obtenerServicios,
  registrarServicio,
} = require('../controllers/servicios');

const { registrarRol, obtenerRol } = require('../controllers/rol');

const router = express.Router();

router
  .route('/channels/:channelName/chaincodes/:chaincodeName')
  .get(obtenerServicios)
  .post(registrarServicio);

module.exports = router;
