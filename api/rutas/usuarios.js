const express = require('express');

const {
  registrarUsuario,
  obtenerUsuario,
  verificarAutorizacion,
} = require('../controllers/usuarios');

const router = express.Router();

router
  .route('/channels/:channelName/chaincodes/:chaincodeName')
  .get(obtenerUsuario)
  .post(registrarUsuario);

router
  .route('/channels/:channelName/chaincodes/:chaincodeName/autorizacion')
  .get(verificarAutorizacion);

module.exports = router;
