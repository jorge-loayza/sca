const path = require('path');
const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');
const colors = require('colors');
const errorHandler = require('./middleware/error');

// Carga de variables de entorno
dotenv.config({ path: './config.env' });

// Rutas
const roles = require('./rutas/roles');
const servicios = require('./rutas/servicios');
const usuarios = require('./rutas/usuarios');

const app = express();

// Body parser
app.use(express.json());

//Middleware para los logs
if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
}

// Mount routers
app.use('/api/v1/roles', roles);
app.use('/api/v1/servicios', servicios);
app.use('/api/v1/usuarios', usuarios);

app.use(errorHandler);

const PORT = process.env.PORT || 5000;

const server = app.listen(
  PORT,
  console.log(
    `Servidor ejecutandose en ${process.env.NODE_ENV} modo el el puerto ${PORT}`
      .yellow.bold
  )
);

// Handle errores de de promesas
process.on('unhandledRejection', (err, promise) => {
  console.log(`Error: ${err.message}`.red);
  server.close(() => process.exit(1));
});
