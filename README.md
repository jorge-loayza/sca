# Sistema de control de acceso

Sistema de control de accesso descentralizado basado en contratos ingeligentes.

## REQUISITOS

Se necesitan lo siguiente correctamente instalado y configurado:

- NodeJS [https://nodejs.org/en/download/]
- Docker [https://docs.docker.com/get-docker/]
- Hyerledger Fabric [https://hyperledger-fabric.readthedocs.io/en/release-2.2/getting_started.html]
- Go [https://golang.org/doc/install]

> Para ejecutar los scripts se necesita agregar Hyperledger al PATH como indica es su documentación.

> Se necesita build-essentian instalado, en ubuntu se realiza con el comando "sudo apt install build-essential", esto es necesario para que el RESTful API mediante el SDK de Hyperledger Fabric gestione todo lo referente con criptografia y comprobación de firmas.
## INSTALACIÓN

### Implementación de la infraestructura.

Para la instalación se siguen los siguientes pasos, es importante que el script se ejecute el el directorio que contiene el mismo:

1. Generar todo el material criptográfico de la red ejecutando el script create-artifacts.sh en la carpeta sca-network/canal

   `$ ./create-artifacts.sh`

2. Levantar toda la red con el script up.sh ubicado en el directorio sca-network/

   `$ ./up.sh`

3. Crear el canal "universidad", unir los peer a ese canal y definir los anchor peers en el directorio sca-network.

   `$ ./crear-canal.sh`

4. Deploy de los Smart contract siguiendo el ciclo de vida definido por Heperledger Fabric. Ejecutar el script deploy-dontracts.sh ubicado en el directorio raiz del proyecto.

   `$ ./deploy-contracts.sh`

> Con estos pasos la infraestructura de la red Blockchan queda instanciada.

### Implementación del endpoint.

Para iniciar el endpoint que se comunicará con la Blockchain tenemos que ejecutar los siguientes comandos estando ubicados en el directorio api del proyecto.

1. Instalar dependencias.

   `$ npm install`

2. Genear el archivo de conexión a la Blockchain ejecutando el script ubicado en el directorio api/config.

   `$ ./generate-ccp.sh`

3. Ejecutar el servicio desde el directorio api.

   `$ npm run start`

Si se muestra el mensaje "found 5 critical severity vulnerabilities", es porque se necesita una validación de firma RSA en algunos packetes necesarios. Solo se tiene que ejecutar el siguiente comando para que intalle estos paquetesÑ

   `$ npm audit fix`

Para ejecutar las pruebas de carga se tiene que hacer lo especificado en [TEST.md](TEST.md)
