# TEST

Pruebas de carga

## REQUISITOS

Se necesita el siguiente programa correctamente instalado y configurado:

- k6 [https://k6.io/docs/getting-started/installation/]

## EJECUCIÓN DE LOS TEST

Para la ejecucion de los test se siguen los siguientes pasos (todos los comandos se ejecutan estando dentro del directorio test del proyecto):

1. Prueba de carga de para la desición de autorización.

   `$ k6 run autorizacion-test.js`

2. Prueba de carga de para la escritura en el ledger.

   `$ k6 run registro-test.js`

3. Prueba de carga de para la lectura del ledger.

   `$ k6 run lectura-test.js`
